#1. Desarrollar un programa que ingrese número y determine si es número par o impar
# (Python-Diagrama) 5 puntos.
'''
A=int(input("Ingrese un numero : "))

if A % 2==0:
    print("El numero es par")

else:
    print("El numero es impar")
'''
#2. Escriba un programa que pida el año actual y un año cualquiera y que
# escriba cuántos años han pasado desde ese año o cuántos años faltan
# para llegar a ese año. (Python-Diagrama) 5 puntos.
'''
AnnioActual=2018

Year=int(input("Ingrese el año deseado : "))
Year1=AnnioActual-Year
Year2=Year-AnnioActual

if Year>AnnioActual:
    print("Han pasado : ",Year2, "años desde el año actual")

else:
    print("Faltan : ",Year1, "años para llegar al año actual")
'''
#3. Cree un programa que pida al usuario su edad y muestre por pantalla la etapa
# en la que se encuentre. (Python -Diagrama) 7 puntos.
#a. 0 a 10 niño(@)
#b. 11 a 18 adolescente
#c. 19 a 64 adulto
#d. 65 en adelante Adulto Mayor
'''
Edad=int(input("Ingrese su edad : "))

if Edad >=0 and Edad <=10:
    print("Eres un niño")
elif Edad >=11 and Edad <=18:
    print("Eres un adolescente")
elif Edad >=19 and Edad <=64:
    print("Eres un adulto")
elif Edad >=65:
    print("Eres un adulto mayor")
else:
    print("La edad ingresada es incorrecta")
'''
#4. Imprima los números del 1 al 20 usando un ciclo while (Python) 4 puntos

'''
num=1

while num<21:
    print(num)
    num=num+1
print("Fin del Ciclo")
'''

#5. Imprima los números del 20 al 1 usando un ciclo for (Python) 4 puntos

'''
for i in range (20,0,-1):
    print(i," ",end="")
'''

# 6. Escriba un programa de alquiler de vehículos que le permita al usuario seleccionar una categoría y
# un modelo de vehículo por una cantidad de días e imprima el monto a pagar a la agencia, el sistema
# debe identificar si el cliente es frecuente o no para aplicarle un descuento y debe cobrar una póliza
# de seguro, además si el cliente requiere el vehículo más tiempo del establecido el costo por día tendrá
# un incremento dependiendo del tipo de vehículo, tenga en cuenta los siguientes parámetros:
# (Python) 45 puntos

Automovil=1,2,3
Hatchback=4,5
Dobletraccion=6,7

Categoria=int(input("Ingrese el tipo de vehículo que desea alquilar : "))
Modelo=int(input("Ingrese el modelo : "))


Cliente=int(input("Es usted un cliente frecuente? :  \n"
                 "1:Sí \n"
                 "0:No \n"))
Dias=int(input("Ingrese la cantidad de días : "))
if Cliente==1:
    if Categoria==1:
        if Modelo<=2015:
            if Dias > 7:
                Precio1=13000
                PrecioExtra1=((Precio1*0.15)+Precio1)
                Dias1=Dias*PrecioExtra1
                Poliza1=((Dias1*0.10)+Dias1)
                Descuento1=((Poliza1*0.3)+Poliza1)
                print("El monto a pagar es de : ",Descuento1)
            elif Dias <= 7:
                PrecioDia=13000
                CantDias=Dias*13000
                Poliza=((CantDias*0.10)+CantDias)
                Descuento=((Poliza*0.3)+Poliza)
                print("El monto a pagar es de : ",Descuento)
        else:
            print("Ese modelo no está disponible")

    if Categoria==2:
        if Modelo>=2001 and Modelo <2014:
            if Dias > 5:
                Precio2=18000
                PrecioExtra2=((Precio2*0.10)+Precio2)
                DiasExtra2=Dias*PrecioExtra2
                Poliza2=((DiasExtra2*0.10)+DiasExtra2)
                Descuento2=((Poliza2*0.3)+Poliza2)
                print("El monto a pagar es de : ",Descuento2)
            elif Dias <= 5:
                PrecioDia=18000
                CantDias=Dias*18000
                Poliza=((CantDias*0.10)+CantDias)
                Descuento=((Poliza*0.3)+Poliza)
                print("El monto a pagar es de : ",Descuento)
        else:
            print("Ese modelo no está disponible")
    if Categoria==3:
        if Modelo>=1990 and Modelo<2000:
            if Dias > 7:
                Precio3=10000
                PrecioExtra3=((Precio3*0.5)+Precio3)
                DiasExtra3=Dias*PrecioExtra3
                Poliza3=((DiasExtra3*0.30)+DiasExtra3)
                Descuento3=((Poliza3*0.10)+Poliza3)
                print("El monto a pagar es de : ",Descuento3)
            elif Dias <= 7:
                PrecioDia=10000
                CantDias=Dias*10000
                Poliza=((CantDias*0.30)+CantDias)
                Descuento=((Poliza*0.10)+Poliza)
                print("El monto a pagar es de : ",Descuento)
        else:
            print("Ese modelo no está disponible")

    if Categoria==4:
        if Modelo>=2015:
            if Dias > 8:
                Precio4=20000
                PrecioExtra4=((Precio4*0.10)+Precio4)
                DiasExtra4=Dias*PrecioExtra4
                Poliza4=((DiasExtra4*0.8)+DiasExtra4)
                Descuento4=((Poliza4*0.5)+Poliza4)
                print("El monto a pagar es de : ",Descuento4)
            elif Dias <= 8:
                PrecioDia=20000
                CantDias=Dias*20000
                Poliza=((CantDias*0.8)+CantDias)
                Descuento=((Poliza*0.5)+Poliza)
                print("El monto a pagar es de : ",Descuento)
        else:
            print("Ese modelo no está disponible")

    if Categoria==5:
        if Modelo>=2000 and Modelo<2014:
            if Dias > 6:
                Precio5=17000
                PrecioExtra5=((Precio5*0.5)+Precio5)
                DiasExtra5=Dias*PrecioExtra5
                Poliza5=((DiasExtra5*0.9)+DiasExtra5)
                Descuento5=((Poliza5*0.3)+Poliza5)
                print("El monto a pagar es de : ",Descuento5)
            elif Dias <= 6:
                PrecioDia=17000
                CantDias=Dias*17000
                Poliza=((CantDias*0.9)+CantDias)
                Descuento=((Poliza*0.3)+Poliza)
                print("El monto a pagar es de : ",Descuento)
        else:
            print("Ese modelo no está disponible")

    if Categoria==6:
        if Modelo>2015:
            if Dias > 7:
                Precio6=20000
                PrecioExtra6=((Precio6*0.15)+Precio6)
                DiasExtra6=Dias*PrecioExtra6
                Poliza6=((DiasExtra6*0.11)+DiasExtra6)
                Descuento6=((Poliza6*0.10)+Poliza6)
                print("El monto a pagar es de : ",Descuento6)
            elif Dias <= 7:
                PrecioDia=20000
                CantDias=Dias*20000
                Poliza=((CantDias*0.11)+CantDias)
                Descuento=((Poliza*0.10)+Poliza)
                print("El monto a pagar es de : ",Descuento)
        else:
            print("Ese modelo no está disponible")

    if Categoria==7:
        if Modelo>2000 and Modelo<=2015:
            if Dias > 4:
                Precio7=16000
                PrecioExtra7=((Precio7*0.5)+Precio7)
                DiasExtra7=Dias*PrecioExtra7
                Poliza7=((DiasExtra7*0.10)+DiasExtra7)
                Descuento7=((Poliza7*0.5)+Poliza7)
                print("El monto a pagar es de : ",Descuento7)
            elif Dias <= 4:
                PrecioDia=16000
                CantDias=Dias*16000
                Poliza=((CantDias*0.10)+CantDias)
                Descuento=((Poliza*0.5)+Poliza)
                print("El monto a pagar es de : ",Descuento)
        else:
            print("Ese modelo no está disponible")

if Cliente!=1:
    if Categoria==1:
        if Modelo<=2015:
            if Dias > 7:
                Precio1=13000
                PrecioExtra1=((Precio1*0.15)+Precio1)
                Dias1=Dias*PrecioExtra1
                Poliza1=((Dias1*0.10)+Dias1)
                print("El monto a pagar es de : ",Poliza1)
            elif Dias <= 7:
                PrecioDia=13000
                CantDias=Dias*13000
                Poliza=((CantDias*0.10)+CantDias)
                print("El monto a pagar es de : ",Poliza)
        else:
            print("Ese modelo no está disponible")

    if Categoria==2:
        if Modelo>=2001 and Modelo <2014:
            if Dias > 5:
                Precio2=18000
                PrecioExtra2=((Precio2*0.10)+Precio2)
                DiasExtra2=Dias*PrecioExtra2
                Poliza2=((DiasExtra2*0.10)+DiasExtra2)
                print("El monto a pagar es de : ",Poliza2)
            elif Dias <= 5:
                PrecioDia=18000
                CantDias=Dias*18000
                Poliza=((CantDias*0.10)+CantDias)
                print("El monto a pagar es de : ",Poliza)
        else:
            print("Ese modelo no está disponible")

    if Categoria==3:
        if Modelo>=1990 and Modelo<2000:
            if Dias > 7:
                Precio3=10000
                PrecioExtra3=((Precio3*0.5)+Precio3)
                DiasExtra3=Dias*PrecioExtra3
                Poliza3=((DiasExtra3*0.30)+DiasExtra3)
                print("El monto a pagar es de : ",Poliza3)
            elif Dias <= 7:
                PrecioDia=10000
                CantDias=Dias*10000
                Poliza=((CantDias*0.30)+CantDias)
                print("El monto a pagar es de : ",Poliza)
        else:
            print("Ese modelo no está disponible")

    if Categoria==4:
        if Modelo>=2015:
            if Dias > 8:
                Precio4=20000
                PrecioExtra4=((Precio4*0.10)+Precio4)
                DiasExtra4=Dias*PrecioExtra4
                Poliza4=((DiasExtra4*0.8)+DiasExtra4)
                print("El monto a pagar es de : ",Poliza4)
            elif Dias <= 8:
                PrecioDia=20000
                CantDias=Dias*20000
                Poliza=((CantDias*0.8)+CantDias)
                print("El monto a pagar es de : ",Poliza)
        else:
            print("Ese modelo no está disponible")

    if Categoria==5:
        if Modelo>=2000 and Modelo<2014:
            if Dias > 6:
                Precio5=17000
                PrecioExtra5=((Precio5*0.5)+Precio5)
                DiasExtra5=Dias*PrecioExtra5
                Poliza5=((DiasExtra5*0.9)+DiasExtra5)
                print("El monto a pagar es de : ",Poliza5)
            elif Dias <= 6:
                PrecioDia=17000
                CantDias=Dias*17000
                Poliza=((CantDias*0.9)+CantDias)
                print("El monto a pagar es de : ",Poliza)
        else:
            print("Ese modelo no está disponible")

    if Categoria==6:
        if Modelo>2015:
            if Dias > 7:
                Precio6=20000
                PrecioExtra6=((Precio6*0.15)+Precio6)
                DiasExtra6=Dias*PrecioExtra6
                Poliza6=((DiasExtra6*0.11)+DiasExtra6)
                print("El monto a pagar es de : ",Poliza6)
            elif Dias <= 7:
                PrecioDia=20000
                CantDias=Dias*20000
                Poliza=((CantDias*0.11)+CantDias)
                print("El monto a pagar es de : ",Poliza)
        else:
            print("Ese modelo no está disponible")

    if Categoria==7:
        if Modelo>2000 and Modelo<=2015:
            if Dias > 4:
                Precio7=16000
                PrecioExtra7=((Precio7*0.5)+Precio7)
                DiasExtra7=Dias*PrecioExtra7
                Poliza7=((DiasExtra7*0.10)+DiasExtra7)
                print("El monto a pagar es de : ",Poliza7)
            elif Dias <= 4:
                PrecioDia=16000
                CantDias=Dias*16000
                Poliza=((CantDias*0.10)+CantDias)
                print("El monto a pagar es de : ",Poliza)
        else:
            print("Ese modelo no está disponible")

print("Gracias por preferirnos")
