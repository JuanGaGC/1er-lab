#1. Desarrollar un programa que ingrese número y determine si es número par o impar
# (Python-Diagrama) 5 puntos.
'''
A=int(input("Ingrese un numero : "))

if A % 2==0:
    print("El numero es par")

else:
    print("El numero es impar")
'''
#2. Escriba un programa que pida el año actual y un año cualquiera y que
# escriba cuántos años han pasado desde ese año o cuántos años faltan
# para llegar a ese año. (Python-Diagrama) 5 puntos.
'''
AnnioActual=2018

Year=int(input("Ingrese el año deseado : "))
Year1=AnnioActual-Year
Year2=Year-AnnioActual

if Year>AnnioActual:
    print("Han pasado : ",Year2, "años desde el año actual")

else:
    print("Faltan : ",Year1, "años para llegar al año actual")
'''
#3. Cree un programa que pida al usuario su edad y muestre por pantalla la etapa
# en la que se encuentre. (Python -Diagrama) 7 puntos.
#a. 0 a 10 niño(@)
#b. 11 a 18 adolescente
#c. 19 a 64 adulto
#d. 65 en adelante Adulto Mayor
'''
Edad=int(input("Ingrese su edad : "))

if Edad >=0 and Edad <=10:
    print("Eres un niño")
elif Edad >=11 and Edad <=18:
    print("Eres un adolescente")
elif Edad >=19 and Edad <=64:
    print("Eres un adulto")
elif Edad >=65:
    print("Eres un adulto mayor")
else:
    print("La edad ingresada es incorrecta")
'''
#4. Imprima los números del 1 al 20 usando un ciclo while (Python) 4 puntos

'''
num=1

while num<21:
    print(num)
    num=num+1
print("Fin del Ciclo")
'''

#5. Imprima los números del 20 al 1 usando un ciclo for (Python) 4 puntos

'''
for i in range (20,0,-1):
    print(i," ",end="")
'''

#6. Escriba un programa de alquiler de vehículos que le permita al usuario seleccionar una categoría y
#un modelo de vehículo por una cantidad de días e imprima el monto a pagar a la agencia, el sistema
#debe identificar si el cliente es frecuente o no para aplicarle un descuento y debe cobrar una póliza
#de seguro, además si el cliente requiere el vehículo más tiempo del establecido el costo por día tendrá
#un incremento dependiendo del tipo de vehículo, tenga en cuenta los siguientes parámetros:
#(Python) 45 puntos




